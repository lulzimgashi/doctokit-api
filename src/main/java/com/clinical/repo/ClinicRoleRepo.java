package com.clinical.repo;


import com.clinical.model.ClinicRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClinicRoleRepo extends JpaRepository<ClinicRole,String> {

    List<ClinicRole> getAllByClinicId(String clinicId);
}
