package com.clinical.repo;
import com.clinical.model.DoctorRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public interface DoctorRoleRepo extends JpaRepository<DoctorRole,String> {

    @Modifying
    @Transactional
    @Query(value = "delete from doctors_roles where doctor_id=:doctorId",nativeQuery = true)
    void dDoctorRoles(@Param("doctorId") String doctorId);


    List<DoctorRole> findAllByDoctorId(String doctorId);
}
