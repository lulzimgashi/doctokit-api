package com.clinical.repo;

import com.clinical.model.Anamnesis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Repository
public interface AnamnesisRepo extends JpaRepository<Anamnesis,String> {


    @Query("select a.clinicId from Anamnesis a where a.id=:id")
    String findClinicId(@Param("id") String id);



    @Modifying
    @Transactional
    @Query(value = "update anamneses a set a.deleted_at=now(),a.user_id=:userId where a.id=:id",nativeQuery = true)
    void dAnamnesis(@Param("userId") String userId, @Param("id") String id);



}
