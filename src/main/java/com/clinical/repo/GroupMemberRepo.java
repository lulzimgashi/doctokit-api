package com.clinical.repo;

import com.clinical.model.GroupMember;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@Repository
public interface GroupMemberRepo extends JpaRepository<GroupMember,String> {

    void deleteAllByUsername(String username);

    @Modifying
    @Transactional
    @Query("delete from GroupMember gm where gm.username=:id")
    void deleteForUser( @Param("id") String username);

}
