package com.clinical.repo;

import com.clinical.model.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Repository
public interface AppointmentRepo extends JpaRepository<Appointment, String> {

    @Query("select a.clinicId from Appointment a where a.id=:id")
    String findClinicId(@Param("id") String id);


    @Modifying
    @Transactional
    @Query(value = "update appointments a set a.deleted_at=now(),a.user_id=:userId where a.id=:id",nativeQuery = true)
    void dAppointment(@Param("userId") String userId, @Param("id") String id);


}
