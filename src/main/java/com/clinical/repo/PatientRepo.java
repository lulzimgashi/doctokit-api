package com.clinical.repo;

import com.clinical.model.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Repository
public interface PatientRepo extends JpaRepository<Patient,String> {


    @Query("select p from Patient p where (p.phone=:phone or p.email=:email) and p.id<>:id")
    List<Patient> findIfPatientExist(@Param("phone") Long phone,@Param("email") String email ,@Param("id") String id);

    @Modifying
    @Transactional
    @Query("update Patient p set p.enabled=0 where p.id=?1")
    void dPatient(String userId);

    @Query("select p.clinicId from Patient p where p.id=:id")
    String findClinicId(@Param("id") String id);

    @Query(value = "select p.* from patients p where (p.email like %:value% or concat_ws(' ', p.first_name,p.last_name) like %:value%  or p.phone like %:value%) and p.clinic_id=:clinicId and p.enabled=1 limit :limit",nativeQuery = true)
    List<Patient> searchPatientsFast(@Param("value") String value,@Param("limit") Integer limit,@Param("clinicId") String clinicId);


    @Query(value = "select p.* from patients p where p.clinic_id=:clinicId and p.enabled=1 limit 20",nativeQuery = true)
    List<Patient> getAllPatients(@Param("clinicId") String clinicId);
}
