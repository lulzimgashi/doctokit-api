package com.clinical.repo;

import com.clinical.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Repository
public interface MessageRepo extends JpaRepository<Message,String> {

    @Query("select m.clinicId from Message m where m.id=:id")
    String findClinicId(@Param("id") String id);


    @Modifying
    @Transactional
    @Query(value = "update messages m set m.deleted_at=now(),m.user_id=:userId where m.id=:id",nativeQuery = true)
    void dMEssage(@Param("userId") String userId, @Param("id") String id);
}
