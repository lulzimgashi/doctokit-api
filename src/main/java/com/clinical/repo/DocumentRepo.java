package com.clinical.repo;

import com.clinical.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Repository
public interface DocumentRepo extends JpaRepository<Document,String> {

    @Modifying
    @Transactional
    @Query(value = "update documents d set d.name=:name,d.label=:label,d.user_id=:userId  where d.id=:id",nativeQuery = true)
    void updateDocument(@Param("name") String name, @Param("label") String label, @Param("id") String id,@Param("userId") String userId);

    @Query("select d.clinicId from Document d where d.id=:id")
    String findClinicId(@Param("id") String id);


}
