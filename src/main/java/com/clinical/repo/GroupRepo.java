package com.clinical.repo;

import com.clinical.model.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@Repository
public interface GroupRepo extends JpaRepository<Group, String> {
}
