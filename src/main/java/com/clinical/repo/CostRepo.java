package com.clinical.repo;

import com.clinical.model.Cost;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Repository
public interface CostRepo extends JpaRepository<Cost,String> {

    @Query("select c.clinicId from Cost c where c.id=:id")
    String findClinicId(@Param("id") String id);



    @Modifying
    @Transactional
    @Query(value = "update costs c set c.deleted_at=now(),c.user_id=:userId where c.id=:id",nativeQuery = true)
    void dCost(@Param("userId") String userId, @Param("id") String id);


}
