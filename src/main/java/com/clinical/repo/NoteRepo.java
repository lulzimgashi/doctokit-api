package com.clinical.repo;

import com.clinical.model.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 18/04/2018.
 */
@Repository
public interface NoteRepo extends JpaRepository<Note,String> {


    @Query("select n.clinicId from Note n where n.id=:id")
    String findClinicId(@Param("id") String id);


    @Modifying
    @Transactional
    @Query(value = "update notes n set n.deleted_at=now(),n.user_id=:userId where n.id=:id",nativeQuery = true)
    void dNote(@Param("userId") String userId, @Param("id") String id);


}
