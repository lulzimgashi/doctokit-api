package com.clinical.repo;

import com.clinical.model.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 22/04/2018.
 */
@Repository
public interface ServiceRepo extends JpaRepository<Service, String> {

    @Modifying
    @Transactional
    @Query("update Service s set s.active=:active,s.name=:name where s.id=:id")
    void update(@Param("active") boolean active,@Param("name") String name,@Param("id") String id);


    @Query("select s.clinicId from Service s where s.id=:id ")
    String findClinicId(@Param("id") String id);

    List<Service> findAllByClinicIdAndDeletedAtIsNull(String clinicId);

    @Modifying
    @Transactional
    @Query(value = "update services s set s.deleted_at=now() where s.id=:id",nativeQuery = true)
    void dService( @Param("id") String id);

}
