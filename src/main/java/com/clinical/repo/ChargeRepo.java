package com.clinical.repo;

import com.clinical.model.Charge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 19/04/2018.
 */
@Repository
public interface ChargeRepo extends JpaRepository<Charge,String> {

    @Query("select ch.clinicId from Charge ch where ch.id=:id")
    String findClinicId(@Param("id") String id);


    @Modifying
    @Transactional
    @Query(value = "update charges ch set ch.deleted_at=now(),ch.user_id=:userId where ch.id=:id",nativeQuery = true)
    void dCharge(@Param("userId") String userId, @Param("id") String id);

}
