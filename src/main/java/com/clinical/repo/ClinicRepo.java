package com.clinical.repo;

import com.clinical.model.Clinic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Repository
public interface ClinicRepo extends JpaRepository<Clinic,String> {

    @Modifying
    @Transactional
    @Query("update Clinic c set c.enabled=0 where c.id=?1")
    void dClinic(String userId);

    Optional<Clinic> findById(String id);

    @Modifying
    @Transactional
    @Query(value = "update clinics c set :col_name = :url where id=:id",nativeQuery = true)
    void updateImg(@Param("id") String id, @Param("url") String url,@Param("col_name" )String column);

}
