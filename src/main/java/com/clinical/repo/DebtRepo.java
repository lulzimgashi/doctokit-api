package com.clinical.repo;

import com.clinical.model.Debt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 19/04/2018.
 */
@Repository
public interface DebtRepo extends JpaRepository<Debt,String> {

    @Query("select d.clinicId from Debt d where d.id=:id")
    String findClinicId(@Param("id") String id);


    @Modifying
    @Transactional
    @Query(value = "update debts d set d.deleted_at=now(),d.user_id=:userId where d.id=:id",nativeQuery = true)
    void dDebt(@Param("userId") String userId, @Param("id") String id);


}
