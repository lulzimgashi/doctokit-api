package com.clinical.repo;

import com.clinical.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@Repository
public interface UserRepo extends JpaRepository<User, String> {

    @Query("select user0_ from User user0_ where (user0_.username=:username or user0_.phone=:phone) and user0_.id<>:id")
    List<User> findIfUserExist(@Param("username") String username, @Param("phone") Long phone, @Param("id") String id);


    @Query("select user.clinicId from User user where username=:username or id=:username")
    String findClinicId(@Param("username") String usernameOrId);

    @Query("select user.id from User user where user.username=:username")
    String findId(@Param("username") String username);

    @Query("select user.password from User user where id=:id")
    String findPassword(@Param("id") String id);

    @Query("select user.username from User user where id=:id")
    String findUsername(@Param("id") String id);

    User findByUsername(String username);

    @Modifying
    @Transactional
    @Query("update User u set u.enabled=0 where u.id=?1")
    void dUser(String userId);


    @Modifying
    @Transactional
    @Query("update User u set u.password=:password where u.id=:id or u.username=:id")
    void changePassword(@Param("password") String password, @Param("id") String userId);


    @Modifying
    @Transactional
    @Query("update User u set u.doctor=:value where u.id=:id")
    void updateDoctor(@Param("id") String id,@Param("value")  Boolean value);
}
