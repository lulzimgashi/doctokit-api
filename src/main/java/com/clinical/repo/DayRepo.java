package com.clinical.repo;

import com.clinical.model.Day;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Repository
public interface DayRepo extends JpaRepository<Day,String> {

    List<Day> findAllByClinicId(String clinicId);

    @Modifying
    @Transactional
    @Query("delete from Day d where d.clinicId=:id")
    void deleteForClinic(@Param("id") String clinicId);

}
