package com.clinical.controller;

import com.clinical.exception.AppointmentExistException;
import com.clinical.model.Appointment;
import com.clinical.model.User;
import com.clinical.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@RestController
@RequestMapping("appointments")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','APPOINTMENT')")
    @RequestMapping(value = "",method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<User>> getAppointments(@RequestParam("date") String date){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        List<User> allAppoinments = appointmentService.getAllAppoinments(authentication, date);
        return new ResponseEntity<>(allAppoinments,HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','APPOINTMENT')")
    @RequestMapping(value = "",method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Appointment> addAppointment(@RequestBody Appointment appointment) throws AppointmentExistException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Appointment saved = appointmentService.addAppointment(authentication, appointment);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','APPOINTMENT')")
    @RequestMapping(value = "",method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Appointment> updateAppointment(@RequestBody Appointment appointment){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Appointment saved = appointmentService.updateAppointment(authentication, appointment);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }



    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','APPOINTMENT')")
    @RequestMapping(value = "/{appointmentId}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteAppointment(@PathVariable("appointmentId") String appointmentId){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        appointmentService.deleteAppointment(authentication, appointmentId);
        return new ResponseEntity<>( HttpStatus.OK);
    }
}
