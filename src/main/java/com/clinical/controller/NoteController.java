package com.clinical.controller;

import com.clinical.model.Note;
import com.clinical.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lulzimgashi on 18/04/2018.
 */
@RestController()
@RequestMapping("notes")
public class NoteController {

    @Autowired
    private NoteService noteService;


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Note> addNote(@RequestBody Note note) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Note saved = noteService.addNote(authentication,note);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Note> updateNote(@RequestBody Note note) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Note saved = noteService.updateNote(authentication,note);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<Note> updateNote(@PathVariable("id") String id) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        noteService.deleteNote(authentication,id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
