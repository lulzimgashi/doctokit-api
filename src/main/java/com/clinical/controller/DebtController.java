package com.clinical.controller;

import com.clinical.model.Debt;
import com.clinical.service.DebtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 19/04/2018.
 */
@RestController
@RequestMapping("debts")
public class DebtController {

    @Autowired
    private DebtService debtService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT','PATIENT')")
    public ResponseEntity<Debt> addDebt(@RequestBody Debt debt){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Debt saved = debtService.addDebt(authentication, debt);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT','PATIENT')")
    public ResponseEntity<Debt> updateDebt(@RequestBody Debt debt){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Debt saved = debtService.updateDebt(authentication, debt);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.GET ,produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT')")
    public ResponseEntity<List<Debt>> getDebtsForDate(@RequestParam("date") String date,
                                                      @RequestParam("type") String type){
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();

        List<Debt> debts= debtService.getDebtsForDate(authentication,type,date);
        return new ResponseEntity<>(debts,HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT','PATIENT')")
    public ResponseEntity<String> deleteDebt(@PathVariable("id") String id){
        Authentication authentication=SecurityContextHolder.getContext().getAuthentication();

        debtService.deleteDebt(authentication,id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
