package com.clinical.controller;

import com.clinical.model.Anamnesis;
import com.clinical.service.AnamnesisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@RestController
@RequestMapping("anamneses")
public class AnamnesisController {

    @Autowired
    private AnamnesisService anamnesisService;


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Anamnesis> addAnamnesis(@RequestBody Anamnesis anamnesis) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Anamnesis saved = anamnesisService.addAnamnesis(authentication,anamnesis);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Anamnesis> updateAnamnesis(@RequestBody Anamnesis anamnesis) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Anamnesis saved = anamnesisService.addAnamnesis(authentication,anamnesis);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteAnamnesis(@PathVariable("id") String id) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        anamnesisService.deleteAnamnesis(authentication,id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.GET)
    public ResponseEntity<List<String>> getAnamneses() {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        List<String> anamneses= anamnesisService.getAnamneses(authentication);
        return new ResponseEntity<>(anamneses,HttpStatus.OK);
    }


}
