package com.clinical.controller;

import com.clinical.exception.BadAuthorityException;
import com.clinical.exception.UserExistException;
import com.clinical.model.PasswordPayload;
import com.clinical.model.User;
import com.clinical.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ResponseEntity<List<User>> getAllUsers(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        List<User> savedUser = userService.getAllUsers(authentication);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','USER')")
    public ResponseEntity<User> addUser(@RequestBody User user) throws BadAuthorityException, UserExistException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User savedUser = userService.addUser(authentication, user);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }



    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','USER')")
    public ResponseEntity<User> updateUser(@RequestBody User user) throws BadAuthorityException, UserExistException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User savedUser = userService.updateUser(authentication, user);
        return new ResponseEntity<>(savedUser, HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','USER')")
    public ResponseEntity<String> deleteUser(@PathVariable("id") String id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        userService.deleteUser(authentication, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/password", method = RequestMethod.PUT)
    public ResponseEntity<String> changeOrResetPassword(@RequestParam("type") String type, @RequestBody PasswordPayload passwordPayload) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if(type.equals("change")){
            userService.changePassword(authentication,passwordPayload);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else if(type.equals("reset")){
            userService.resetPassword(passwordPayload);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "login", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<User> getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = userService.getUser(authentication);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }




}
