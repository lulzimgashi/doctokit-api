package com.clinical.controller;

import com.clinical.exception.PatientExistException;
import com.clinical.model.Patient;
import com.clinical.model.PatientSearchPayload;
import com.clinical.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@RestController
@RequestMapping("patients")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.POST,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Patient> addPatient(@RequestBody Patient patient) throws PatientExistException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Patient saved = patientService.addPatient(authentication,patient);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "",method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE},produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Patient> updatePatient(@RequestBody Patient patient) throws PatientExistException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Patient saved = patientService.updatePatient(authentication,patient);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "/{patientId}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deletePatient(@PathVariable("patientId") String patientId) throws PatientExistException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        patientService.deletePatient(authentication,patientId);
        return new ResponseEntity<>( HttpStatus.OK);
    }


    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "/{patientId}",method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Patient> getPatient(@PathVariable("patientId") String patientId) {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Patient patient = patientService.getPatient(authentication, patientId);
        return new ResponseEntity<>(patient, HttpStatus.OK);
    }



    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    @RequestMapping(value = "/search",method = RequestMethod.POST,produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Patient>> searchPatient(@RequestBody PatientSearchPayload search, @RequestParam("limit") Integer limit) throws PatientExistException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        if(search.getKeyword().length()>2) {
            List<Patient> patientList = patientService.searchPatientFast(authentication,limit,search.getKeyword());
            return new ResponseEntity<>(patientList, HttpStatus.OK);
        }else{
            List<Patient> patientList = patientService.searchAllPatients(authentication);
            return new ResponseEntity<>(patientList, HttpStatus.OK);
        }
    }
}
