package com.clinical.controller;

import com.clinical.exception.FileNotUploadedException;
import com.clinical.model.Clinic;
import com.clinical.model.Day;
import com.clinical.model.Document;
import com.clinical.service.ClinicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@RestController
@RequestMapping("clinics")
public class ClinicController {

    @Autowired
    private ClinicService clinicService;


    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Clinic> addClinic(@RequestBody Clinic clinic) {
        Clinic saved = clinicService.addClinic(clinic);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Clinic> updateClinic(@RequestBody Clinic clinic) {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Clinic saved = clinicService.updateClinic(authentication, clinic);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    @RequestMapping(value = "/day", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> updateClinicDays(@RequestBody Day day) throws Exception {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        clinicService.updateClinicDays(authentication, day);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/{clinicId}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Clinic> getClinic() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Clinic saved = clinicService.getClinic(authentication);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('SUPER_ADMIN')")
    @RequestMapping(value = "/{clinicId}", method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteClinic(@PathVariable("clinicId") String clinicId) {
        clinicService.deleteClinic(clinicId);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/img", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    public ResponseEntity<String> addDocument(@RequestParam("type") String name,
                                              @RequestParam("file") MultipartFile file) throws FileNotUploadedException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        String saved = clinicService.addImage(authentication, name,file);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }
}
