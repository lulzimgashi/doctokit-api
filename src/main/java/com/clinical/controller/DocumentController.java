package com.clinical.controller;

import com.clinical.exception.FileNotUploadedException;
import com.clinical.model.Document;
import com.clinical.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@RestController
@RequestMapping("documents")
public class DocumentController {

    @Autowired
    private DocumentService documentService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST','DEBT','PATIENT')")
    public ResponseEntity<Document> addDocument(@RequestParam("name") String name,
                                            @RequestParam("file") MultipartFile file,
                                            @RequestParam("label") String label,
                                            @RequestParam("date") String date,
                                            @RequestParam("parentId") String parentId,
                                            @RequestParam("parentName") String parentName) throws FileNotUploadedException {
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Document saved = documentService.addDocument(authentication, name,file,label,parentId,parentName,date);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT,consumes = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST','DEBT','PATIENT')")
    public ResponseEntity<String> updateDocument(@RequestBody Document document){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();


        documentService.updateDocument(authentication,document);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST','DEBT','PATIENT')")
    public ResponseEntity<String> delete(@PathVariable("id") String id){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();


        documentService.deleteDocument(authentication,id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
