package com.clinical.controller;

import com.clinical.model.ClinicRole;
import com.clinical.service.DoctorService;
import com.clinical.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("doctors")
public class DoctorController {

    @Autowired
    private UserService userService;

    @Autowired
    private DoctorService doctorService;


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','USER')")
    public ResponseEntity<String> updateDoctorStatus(@PathVariable("id") String id, @RequestParam("value") Boolean value) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        userService.updateDoctor(authentication, id,value);
        return new ResponseEntity<>( HttpStatus.OK);
    }


    @RequestMapping(value = "/roles/{doctorId}", method = RequestMethod.POST)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','USER')")
    public ResponseEntity<String> updateDoctorRoles(@PathVariable String doctorId,@RequestBody List<String> doctorRolesTranslationsIds) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        doctorService.updateDoctorRoles(authentication,doctorRolesTranslationsIds,doctorId);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/roles", method = RequestMethod.GET)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','USER')")
    public ResponseEntity<List<ClinicRole>> getDoctorRolesForClinic() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


        List<ClinicRole> clinicRoles =  doctorService.getDoctorRolesForClinic(authentication);

        return new ResponseEntity<>(clinicRoles,HttpStatus.OK);
    }
}
