package com.clinical.controller;

import com.clinical.model.Service;
import com.clinical.service.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lulzimgashi on 22/04/2018.
 */
@RestController
@RequestMapping("services")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    public ResponseEntity<Service> addService(@RequestBody Service service) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        Service savedService = serviceService.addService(authentication, service);
        return new ResponseEntity<>(savedService, HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    public ResponseEntity<Service> updateService(@RequestBody Service service) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        serviceService.updateService(authentication, service);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN')")
    public ResponseEntity<String> deleteService(@PathVariable String id) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        serviceService.deleteService(authentication, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
