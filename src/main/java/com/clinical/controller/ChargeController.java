package com.clinical.controller;

import com.clinical.model.Charge;
import com.clinical.service.ChargeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lulzimgashi on 19/04/2018.
 */
@RestController
@RequestMapping("charges")
public class ChargeController {

    @Autowired
    private ChargeService chargeService;


    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT','PATIENT')")
    public ResponseEntity<Charge> addCharge(@RequestBody Charge charge){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Charge saved = chargeService.addCharge(authentication, charge);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT','PATIENT')")
    public ResponseEntity<Charge> updateCharge(@RequestBody Charge charge){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Charge saved = chargeService.updateCharge(authentication, charge);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','DEBT','PATIENT')")
    public ResponseEntity<Charge> deleteCharge(@PathVariable("id") String id){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        chargeService.deleteCharge(authentication, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
