package com.clinical.controller;


import com.clinical.model.Message;
import com.clinical.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@RestController
@RequestMapping("messages")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    public ResponseEntity<Message> addMessage(@RequestBody Message message){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Message saved = messageService.addMessage(authentication, message);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','PATIENT')")
    public ResponseEntity<String> deleteMessage(@PathVariable("id") String id){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        messageService.deleteMessage(authentication, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
