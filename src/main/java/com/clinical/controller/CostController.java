package com.clinical.controller;

import com.clinical.model.Cost;
import com.clinical.service.CostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@RestController
@RequestMapping("costs")
public class CostController {

    @Autowired
    private CostService costService;

    @RequestMapping(value = "", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST')")
    public ResponseEntity<Cost> addDebt(@RequestBody Cost debt){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Cost saved = costService.addCost(authentication, debt);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @RequestMapping(value = "", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST')")
    public ResponseEntity<Cost> updateDebt(@RequestBody Cost debt){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        Cost saved = costService.updateCost(authentication, debt);
        return new ResponseEntity<>(saved, HttpStatus.OK);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST')")
    public ResponseEntity<String> updateDebt(@PathVariable("id") String id){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        costService.deleteCost(authentication, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "", method = RequestMethod.GET,produces = {MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasAnyRole('SUPER_ADMIN','ADMIN','COST')")
    public ResponseEntity<List<Cost>> getCosts(@RequestParam("date") String date){
        Authentication authentication= SecurityContextHolder.getContext().getAuthentication();

        List<Cost> allCostsForDate = costService.getAllCostsForDate(authentication, date);
        return new ResponseEntity<>(allCostsForDate,HttpStatus.OK);
    }
}
