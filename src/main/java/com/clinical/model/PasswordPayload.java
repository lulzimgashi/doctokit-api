package com.clinical.model;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
public class PasswordPayload {
    String userId;
    String password;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
