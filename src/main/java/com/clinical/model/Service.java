package com.clinical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by lulzimgashi on 22/04/2018.
 */
@Entity
@Table(name = "services")
public class Service {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "char(36)")
    private String id;

    @Column(columnDefinition = "char(50)")
    private String name;

    @Column(columnDefinition = "integer(1) default 1", nullable = false)
    private Boolean active;

    @Column(columnDefinition = "datetime")
    private String deletedAt;

    @Column(columnDefinition = "char(36)")
    private String clinicId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }
}
