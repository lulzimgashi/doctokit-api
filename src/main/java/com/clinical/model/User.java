package com.clinical.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "char(36)")
    private String id;

    @Column(columnDefinition = "char(20)", nullable = false)
    private String firstName;

    @Column(columnDefinition = "char(20)", nullable = false)
    private String lastName;

    @Column(columnDefinition = "char(30)", nullable = false,unique = true)
    private String email;

    @Column(columnDefinition = "char(30)", nullable = false,unique = true)
    private String username;

    @Column(columnDefinition = "char(60) binary", nullable = false)
    private String password;

    @Column(columnDefinition = "integer(1) default 1", nullable = false)
    private Boolean enabled;

    @Column(columnDefinition = "integer(1) default 1", nullable = false)
    private Boolean doctor;

    @Column(columnDefinition = "char(36)", nullable = false)
    private String clinicId;

    @Column(columnDefinition = "int(12)", nullable = false, unique = true)
    private Long phone;

    @Transient
    private List<String> authorities;

    @Transient
    private List<Appointment> appointments;

    @Transient
    private List<ClinicRole> clinicRoles;

    @Transient
    private Clinic clinic;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public List<String> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<String> authorities) {
        this.authorities = authorities;
    }

    public Boolean getDoctor() {
        return doctor;
    }

    public void setDoctor(Boolean doctor) {
        this.doctor = doctor;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public List<ClinicRole> getClinicRoles() {
        return clinicRoles;
    }

    public void setClinicRoles(List<ClinicRole> clinicRoles) {
        this.clinicRoles = clinicRoles;
    }
}
