package com.clinical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Entity
@Table(name = "documents")
public class Document {

    @Id
    @Column(columnDefinition = "char(36)")
    private String id;

    @Column(columnDefinition = "char(10)")
    private String type;

    @Column(columnDefinition = "char(20)")
    private String name;

    @Column(columnDefinition = "char(20)")
    private String label;

    @Column(columnDefinition = "datetime default now()")
    private String date;

    @Column(columnDefinition = "char(36)")
    private String patientId;

    @Column(columnDefinition = "char(36)")
    private String debtId;

    @Column(columnDefinition = "char(36)")
    private String costId;

    @Column(columnDefinition = "char(36)")
    private String userId;

    @Column(columnDefinition = "char(36)")
    private String clinicId;


    @Column(columnDefinition = "varchar(1000)")
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getDebtId() {
        return debtId;
    }

    public void setDebtId(String debtId) {
        this.debtId = debtId;
    }

    public String getCostId() {
        return costId;
    }

    public void setCostId(String costId) {
        this.costId = costId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

