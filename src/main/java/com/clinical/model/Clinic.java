package com.clinical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@Entity
@Table(name = "clinics")
public class Clinic {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "char(36)")
    private String id;

    @Column(columnDefinition = "char(20)", nullable = false)
    private String name;

    @Column(columnDefinition = "char(100)")
    private String description;

    @Column(columnDefinition = "char(30)", nullable = false)
    private String email;

    @Column(columnDefinition = "char(50)", nullable = false)
    private String address;

    @Column(columnDefinition = "char(80)", nullable = false)
    private String location;

    @Column(columnDefinition = "int(12)", nullable = false)
    private Long phone;

    @Column(columnDefinition = "char(50)")
    private String website;

    @Column(columnDefinition = "varchar(1000)")
    private String profileImage;

    @Column(columnDefinition = "varchar(1000)")
    private String coverImage;

    @Column(columnDefinition = "char(3)")
    private String language;

    @Column(columnDefinition = "char(20)")
    private String type;

    @Column(columnDefinition = "integer(1) default 1", nullable = false)
    private Boolean sms;

    @Column(columnDefinition = "integer(1) default 1", nullable = false)
    private Boolean enabled;

    @Column(columnDefinition = "integer default 30", nullable = false)
    private Integer slotTime;

    @Column(nullable = false)
    private Double longitude;

    @Column(nullable = false)
    private Double latitude;

    @Transient
    private List<Day> days;

    @Transient
    private List<Service> services;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Boolean getSms() {
        return sms;
    }

    public void setSms(Boolean sms) {
        this.sms = sms;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Integer getSlotTime() {
        return slotTime;
    }

    public void setSlotTime(Integer slotTime) {
        this.slotTime = slotTime;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public List<Day> getDays() {
        return days;
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }
}
