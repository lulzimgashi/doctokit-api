package com.clinical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by lulzimgashi on 18/04/2018.
 */
@Entity
@Table(name = "notes")
public class Note {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "varchar(36)")
    private String id;

    @Column(columnDefinition = "varchar(300)")
    private String text;

    @Column(columnDefinition = "datetime")
    private String date;

    @Column(columnDefinition = "datetime")
    private String deletedAt;

    @Column(columnDefinition = "varchar(36)")
    private String clinicId;

    @Column(columnDefinition = "varchar(36)")
    private String patientId;

    @Column(columnDefinition = "varchar(36)")
    private String serviceId;

    @Column(columnDefinition = "varchar(36)")
    private String userId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
