package com.clinical.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Entity
@Table(name = "patients")
public class Patient {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "char(36)")
    private String id;

    @Column(columnDefinition = "char(20)", nullable = false)
    private String firstName;

    @Column(columnDefinition = "char(20)", nullable = false)
    private String lastName;

    @Column(columnDefinition = "char(20)")
    private String email;

    @Column(columnDefinition = "char(12)", nullable = false, unique = true)
    private Long phone;

    @Column(columnDefinition = "char(6)", nullable = false)
    private String gender;

    @Column(columnDefinition = "char(10)")
    private String birthDate;

    @Column(columnDefinition = "char(50)")
    private String address;

    @Column(columnDefinition = "char(36)")
    private String clinicId;

    @Column(columnDefinition = "integer(1) default 1", nullable = false)
    private Boolean enabled;


    @Transient
    private List<Appointment> appointments;

    @Transient
    private List<Note> notes;

    @Transient
    private List<Document> documents;

    @Transient
    private List<Debt> debts;

    @Transient
    private List<Anamnesis> anamneses;

    @Transient
    private List<Message> messages;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

    public List<Note> getNotes() {
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    public List<Debt> getDebts() {
        return debts;
    }

    public void setDebts(List<Debt> debts) {
        this.debts = debts;
    }

    public List<Anamnesis> getAnamneses() {
        return anamneses;
    }

    public void setAnamneses(List<Anamnesis> anamneses) {
        this.anamneses = anamneses;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }


}
