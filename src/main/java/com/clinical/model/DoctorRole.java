package com.clinical.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "doctors_roles")
public class DoctorRole {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition = "char(36)")
    private String id;

    @Column(columnDefinition = "char(36)")
    private String doctorId;

    @Column(columnDefinition = "char(36)")
    private String roleTranslationId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getRoleTranslationId() {
        return roleTranslationId;
    }

    public void setRoleTranslationId(String roleTranslationId) {
        this.roleTranslationId = roleTranslationId;
    }
}
