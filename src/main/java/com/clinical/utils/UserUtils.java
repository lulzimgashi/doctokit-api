package com.clinical.utils;

import com.clinical.exception.BadAuthorityException;
import com.clinical.model.GroupMember;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
public class UserUtils {

    public static boolean isSuperAdmin(Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        boolean isSuperAdmin = false;
        for (GrantedAuthority authority : authorities) {
            if (authority.getAuthority().equals("ROLE_SUPER_ADMIN")) {
                isSuperAdmin = true;
            }
        }
        return isSuperAdmin;
    }

    public static void checkAuthorities(List<String> authorities) throws BadAuthorityException {

        if (authorities.contains("SUPER_ADMIN")
                || authorities.contains("ADMIN")
                || authorities.contains("REPORT")
                || authorities.contains("APPOINTMENT")
                || authorities.contains("DEBT")
                || authorities.contains("COST")
                || authorities.contains("USER")
                || authorities.contains("PATIENT")) {
            return;
        } else {
            throw new BadAuthorityException(ConstValues.BAD_AUTHORITY_EXCEPTION);
        }

    }

    public static List<GroupMember> modifyAuthorities(List<String> authorities, String username) {
        List<GroupMember> groupMembers = new ArrayList<>();

        for (String authority : authorities) {
            GroupMember groupMember = new GroupMember();
            groupMember.setUsername(username);

            if (authority.equals("SUPER_ADMIN")) {
                groupMember.setGroup_id("1");
            }
            if (authority.equals("ADMIN")) {
                groupMember.setGroup_id("2");
            }
            if (authority.equals("REPORT")) {
                groupMember.setGroup_id("3");
            }
            if (authority.equals("APPOINTMENT")) {
                groupMember.setGroup_id("7");
            }
            if (authority.equals("DEBT")) {
                groupMember.setGroup_id("5");
            }
            if (authority.equals("COST")) {
                groupMember.setGroup_id("4");
            }
            if (authority.equals("USER")) {
                groupMember.setGroup_id("6");
            }
            if (authority.equals("PATIENT")) {
                groupMember.setGroup_id("8");
            }
            groupMembers.add(groupMember);
        }

        return groupMembers;
    }
}
