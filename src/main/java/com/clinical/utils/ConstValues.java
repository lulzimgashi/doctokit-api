package com.clinical.utils;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
public class ConstValues {
    //user
    public static final String USER_WITH_SAME_FIELD= "User with the same %s %s exist,please chose another %s";
    public static final String CANNOT_ADD_WITH_ROLE="You can't add new user with %s role";
    public static final String BAD_AUTHORITY_EXCEPTION="You are trying to save an authority which doesn't exist";
    public static final String CANNOT_EDIT_USER_EXCEPTION="You can't edit that user";
    public static final String CANNOT_DELETE_USER_EXCEPTION="You can't delete that user";

    //clinic
    public static final String CANNOT_EDIT_CLINIC_EXCEPTION="You can't edit that clinic";
    public static final String CANNOT_DELETE_CLINIC_EXCEPTION="You can't delete that clinic";

    //patient
    public static final String CANNOT_EDIT_PATIENT_EXCEPTION="You can't edit that patient";
    public static final String CANNOT_DELETE_PATIENT_EXCEPTION="You can't edit that patient";
    public static final String PATIENT_WITH_SAME_FIELD= "Patient with the same %s %s exist,please chose another %s";
    
    //appointment
    public static final String CANNOT_EDIT_APPOINTMENT_EXCEPTION="You can't edit that appointment";
    public static final String CANNOT_DELETE_APPOINTMENT_EXCEPTION="You can't delete that appointment";
    public static final String WRONG_START_AND_END_TIME="Wrong start and end time";
    public static final String WRONG_START_TIME="Please chose anther start time";
    public static final String WRONG_END_TIME="Please chose another end time";

    //note
    public static final String CANNOT_EDIT_NOTE_EXCEPTION="You can't edit that note";
    public static final String CANNOT_DELETE_NOTE_EXCEPTION="You can't delete that note";

    //debt
    public static final String CANNOT_EDIT_DEBT_EXCEPTION="You can't edit that debt";
    public static final String CANNOT_DELETE_DEBT_EXCEPTION="You can't delete that debt";

    //charge
    public static final String CANNOT_EDIT_CHARGE_EXCEPTION="You can't edit that charge";
    public static final String CANNOT_DELETE_CHARGE_EXCEPTION="You can't delete that charge";


    //cost
    public static final String CANNOT_EDIT_COST_EXCEPTION="You can't edit that cost";
    public static final String CANNOT_DELETE_COST_EXCEPTION="You can't delete that cost";


    //documents
    public static final String CANNOT_DELETE_DOCUMENT_EXCEPTION="You can't delete that document";


    //messages
    public static final String CANNOT_DELETE_MESSAGE_EXCEPTION="You can't delete that message";


    //services
    public static final String CANNOT_EDIT_SERVICE_EXCEPTION="You can't edit that service";
}
