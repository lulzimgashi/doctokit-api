package com.clinical.exception;

/**
 * Created by lulzimgashi on 16/04/2018.
 */
public class AppointmentExistException extends Throwable {

    public AppointmentExistException(String message) {
        super(message);
    }
}
