package com.clinical.exception;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
public class BadAuthorityException extends Throwable {

    public BadAuthorityException(String message) {
        super(message);
    }
}
