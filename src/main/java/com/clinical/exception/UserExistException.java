package com.clinical.exception;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
public class UserExistException extends Throwable {
    public UserExistException(String message) {
        super(message);
    }
}
