package com.clinical.exception;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
public class FileNotUploadedException extends Throwable {

    public FileNotUploadedException(String message) {
        super(message);
    }
}
