package com.clinical.exception;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
public class PatientExistException extends Throwable {
    public PatientExistException(String message) {
        super(message);
    }
}
