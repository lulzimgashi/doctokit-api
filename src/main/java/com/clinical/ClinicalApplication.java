package com.clinical;

import com.clinical.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicalApplication implements ApplicationRunner {

	@Autowired
	private UploadService uploadService;



	public static void main(String[] args) {
		SpringApplication.run(ClinicalApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments applicationArguments) throws Exception {
		uploadService.init();
	}
}
