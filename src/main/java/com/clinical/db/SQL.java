package com.clinical.db;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
public class SQL {
    public static final String GET_ALL_USERS_FOR_CLINIC="SELECT u.id, u.first_name, u.last_name, u.email, u.phone,u.doctor, u.enabled, ga.authority FROM users u INNER JOIN group_members gm ON u.username = gm.username INNER JOIN group_authorities ga ON gm.group_id = ga.id WHERE u.clinic_id = ? ORDER BY u.username;";

    public static final String GET_ALL_NOTES_FOR_PATIENT="SELECT n.service_id, n.id, n.date, n.text, n.user_id FROM notes n INNER JOIN patients p ON n.patient_id = p.id WHERE n.deleted_at IS NULL AND n.patient_id = :patientId AND n.clinic_id = :clinicId";

    public static final String GET_ALL_DOCUMENTS_FOR_PATIENT="SELECT d.id, d.label, d.name, d.type, d.url, d.date, d.user_id FROM documents d INNER JOIN patients p ON d.patient_id = p.id WHERE d.patient_id=:patientId AND d.clinic_id=:clinicId;";

    public static final String GET_ALL_DEBTS_FOR_PATIENT="SELECT d.*, ch.* FROM debts d LEFT JOIN charges ch ON d.id = ch.debt_id AND ch.deleted_at IS NULL WHERE d.deleted_at IS NULL AND d.patient_id=:patientId AND d.clinic_id=:clinicId";

    public static final String GET_ALL_ANAMNESES_FOR_PATIENT="SELECT a.id, a.date, a.name, a.user_id FROM anamneses a INNER JOIN patients p ON a.patient_id = p.id WHERE a.deleted_at IS NULL AND a.patient_id=:patientId AND a.clinic_id=:clinicId; ";

    public static final String GET_ALL_MESSAGES_FOR_PATIENT="SELECT m.id, m.date, m.phone, m.text, m.user_id FROM messages m INNER JOIN patients p ON m.patient_id = p.id WHERE m.deleted_at IS NULL AND m.patient_id=:patientId AND m.clinic_id=:clinicId;";

    public static final String GET_PATIENT_WITH_ID="SELECT p.id,p.gender,p.enabled, p.address, p.birth_date, p.email, p.first_name, p.last_name, p.phone, a.id, a.created_at, a.date, a.start, a.end, a.description, a.doctor_id,a.user_id FROM patients p LEFT JOIN appointments a ON a.patient_id = p.id AND a.deleted_at IS NULL WHERE p.id = :id AND p.clinic_id =:clinicId AND p.enabled = 1";

    public static final String GET_ALL_APPOINTMENTS_FOR_CLINIC_SQL="SELECT a.user_id, u.id, u.first_name, u.last_name, u.email, u.doctor, u.enabled, a.id, a.date, a.created_at, a.start, a.end, a.patient_id,a.doctor_id, p.first_name, p.last_name, p.phone,p.gender FROM users u LEFT JOIN appointments a ON(a.doctor_id = u.id) AND date(a.date) BETWEEN date(:date) AND date_add(:date, INTERVAL 4 DAY) AND a.deleted_at IS NULL LEFT JOIN patients p ON a.patient_id = p.id WHERE u.clinic_id = :clinicId AND (a.id IS NOT NULL OR u.doctor = 1) ORDER BY u.id;";

    public static final String CHECK_APPOINTMENT_START_END_TIME="SELECT count(*) FROM appointments ap WHERE ap.doctor_id =:doctorId AND ap.deleted_at IS NULL AND ((ap.start > :startTime and ap.start < :endTime ) OR (ap.end > :startTime and ap.end < :endTime  )) and ap.clinic_id=:clinicId and DATE(ap.date)=:date AND ap.id!=:id";

    public static final String CHECK_APPOINTMENT_START_TIME="SELECT count(*) FROM appointments ap WHERE ap.doctor_id =? AND ap.deleted_at IS NULL AND (? >= ap.start AND ? < ap.end) and ap.clinic_id=? and DATE(ap.date)=? AND ap.id!=?";

    public static final String CHECK_APPOINTMENT_END_TIME="SELECT count(*) FROM appointments ap WHERE ap.doctor_id =? AND ap.deleted_at IS NULL AND (? > ap.start AND ? <= ap.end) and ap.clinic_id=? and DATE(ap.date)=? AND ap.id!=?";

    public static final String GET_ALL_DEBTS_FOR_CLINIC_SQL="SELECT d.id, d.amount, d.description, d.name, d.phone, d.start_date, d.end_date, d.patient_id, ch.id, ch.date, ch.amount, ch.description FROM debts d LEFT JOIN charges ch ON ch.debt_id = d.id AND ch.deleted_at IS NULL AND ch.clinic_id = :clinicId WHERE d.deleted_at IS NULL AND if(:type = 'start', date(d.start_date) = date(:date), date(d.end_date) = date(:date)) AND d.clinic_id = :clinicId ORDER BY d.id,d.start_date;";

    public static final String GET_ALL_COSTS_FOR_CLINIC_SQL="SELECT c.id, c.amount, c.date, c.description, c.name, c.phone,c.user_id FROM costs c WHERE c.deleted_at IS NULL AND date(c.date)=date(:date) AND c.clinic_id=:clinicId;";

    public static final String GET_CLINIC_BY_ID= "select c.*,d.* from clinics c INNER JOIN days d ON c.id=d.clinic_id WHERE c.id =?;";

    public static final String GET_AUTHORITIES_FOR_USER="select ga.* from group_members gm INNER JOIN group_authorities ga ON ga.group_id=gm.group_id WHERE gm.username=?";

    public static final String GET_ALL_ANAMNESES_FOR_CLINIC="select DISTINCT a.name FROM anamneses a where a.clinic_id=:clinicId;";

    public static final String GET_ALL_DOCTOR_ROLES_FOR_DOCTOR = "SELECT cr.*, dr.* FROM doctors_roles dr INNER JOIN clinic_roles cr ON dr.role_translation_id=cr.translation_id WHERE dr.doctor_id IN(:doctor_ids);";
}
