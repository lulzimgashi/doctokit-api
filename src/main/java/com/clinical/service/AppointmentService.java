package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.exception.AppointmentExistException;
import com.clinical.model.Appointment;
import com.clinical.model.CustomUser;
import com.clinical.model.User;
import com.clinical.repo.AppointmentRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Service
public class AppointmentService {

    @Autowired
    private AppointmentRepo appointmentRepo;

    @Autowired
    private DoctorService doctorService;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public Appointment addAppointment(Authentication authentication,Appointment appointment) throws AppointmentExistException {
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String userId = ((CustomUser)authentication.getPrincipal()).getId();

        appointment.setClinicId(clinicId);
        appointment.setUserId(userId);


        checkIfStartTimeExist(appointment.getStart(),clinicId, appointment.getDate(), appointment.getId(), appointment.getDoctorId());
        checkIfEndTimeExist(appointment.getEnd(),clinicId, appointment.getDate(), appointment.getId(), appointment.getDoctorId());
        checkForWrongStartTimeAndEndTime(appointment.getStart(), appointment.getEnd(), clinicId, appointment.getDate(), appointment.getId(), appointment.getDoctorId());

        return appointmentRepo.save(appointment);
    }


    @Transactional
    public Appointment updateAppointment(Authentication authentication,Appointment appointment){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();

        if(!clinicId.equals(appointment.getClinicId())){
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_APPOINTMENT_EXCEPTION);
        }
        String userId =((CustomUser)authentication.getPrincipal()).getId();

        appointment.setUserId(userId);
        appointment.setClinicId(clinicId);

        return appointmentRepo.save(appointment);
    }


    @Transactional
    public void deleteAppointment(Authentication authentication, String appointmentId) {
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        if(!clinicId.equals(appointmentRepo.findClinicId(appointmentId))){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_APPOINTMENT_EXCEPTION);
        }

        String userId = ((CustomUser)authentication.getPrincipal()).getId();

        appointmentRepo.dAppointment(userId,appointmentId);
    }

    @Transactional(readOnly = true)
    public List<User> getAllAppoinments(Authentication authentication, String date){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        Map<String, Object> map = new HashMap<>();
        map.put("date",date);
        map.put("clinicId",clinicId);

        List<User> queryResult = namedParameterJdbcTemplate.query(SQL.GET_ALL_APPOINTMENTS_FOR_CLINIC_SQL, map, resultSet -> {
            List<User> doctors = new ArrayList<>();

            String last="";
            while(resultSet.next()){
                if(!last.equals(resultSet.getString("u.id"))){
                    User user=new User();
                    user.setId(resultSet.getString("u.id"));
                    user.setFirstName(resultSet.getString("u.first_name"));
                    user.setLastName(resultSet.getString("u.last_name"));
                    user.setEmail(resultSet.getString("u.email"));
                    user.setDoctor(resultSet.getBoolean("u.doctor"));
                    user.setEnabled(resultSet.getBoolean("u.enabled"));
                    user.setAppointments(new ArrayList<>());
                    user.setClinicId(clinicId);


                    doctors.add(user);
                }

                if(resultSet.getString("a.id") != null) {
                    Appointment appointment = new Appointment();
                    appointment.setId(resultSet.getString("a.id"));
                    appointment.setDate(resultSet.getString("a.date"));
                    appointment.setCreatedAt(resultSet.getString("a.created_at"));
                    appointment.setStart(resultSet.getString("a.start"));
                    appointment.setEnd(resultSet.getString("a.end"));
                    appointment.setPatientId(resultSet.getString("a.patient_id"));
                    appointment.setDoctorId(resultSet.getString("a.doctor_id"));
                    appointment.setFirstName(resultSet.getString("p.first_name"));
                    appointment.setLastName(resultSet.getString("p.last_name"));
                    appointment.setPhone(resultSet.getLong("p.phone"));
                    appointment.setGender(resultSet.getString("p.gender"));
                    appointment.setUserId(resultSet.getString("a.user_id"));

                    appointment.setClinicId(clinicId);

                    doctors.get(doctors.size()-1).getAppointments().add(appointment);
                }

                last=resultSet.getString("u.id");
            }


            for (int i = 0; i < doctors.size(); i++) {
                if(!doctors.get(i).getEnabled() && doctors.get(i).getAppointments().size()==0){
                    doctors.remove(i);
                }
            }


            doctorService.getDoctorRoles(doctors);


            return doctors;
        });


        return queryResult;
    }



    private void checkForWrongStartTimeAndEndTime(String startTime, String endTime, String clinicId, String date, String appointmentId, String doctorId) throws AppointmentExistException {
        Map<String, Object> map = new HashMap<>();
        map.put("startTime", startTime);
        map.put("endTime", endTime);
        map.put("clinicId", clinicId);
        map.put("date", date);
        appointmentId = appointmentId == null ? "" : appointmentId;
        map.put("id", appointmentId);
        map.put("doctorId", doctorId);

        Integer integer = namedParameterJdbcTemplate.queryForObject(SQL.CHECK_APPOINTMENT_START_END_TIME, map, Integer.class);
        if (integer != null && integer > 0) {
            throw new AppointmentExistException(ConstValues.WRONG_START_AND_END_TIME);
        }
    }

    private void checkIfStartTimeExist(String startTime, String clinicId, String date, String appointmentId, String doctorId) throws AppointmentExistException {
        appointmentId = appointmentId == null ? "" : appointmentId;

        Integer integer = this.jdbcTemplate.queryForObject(SQL.CHECK_APPOINTMENT_START_TIME, new Object[]{doctorId, startTime, startTime, clinicId, date, appointmentId}, Integer.class);
        if (integer != null && integer > 0) {
            throw new AppointmentExistException(ConstValues.WRONG_START_TIME);
        }
    }

    private void checkIfEndTimeExist(String endTime, String clinicId, String date, String appointmentId, String doctorId) throws AppointmentExistException {
        appointmentId = appointmentId == null ? "" : appointmentId;
        Integer integer = this.jdbcTemplate.queryForObject(SQL.CHECK_APPOINTMENT_END_TIME, new Object[]{doctorId, endTime, endTime, clinicId, date, appointmentId}, Integer.class);
        if (integer != null && integer > 0) {
            throw new AppointmentExistException(ConstValues.WRONG_END_TIME);
        }
    }


    @Transactional(readOnly = true)
    public List<Appointment> addAppointmentFromPatient(List<Appointment> appointments) {

        return appointmentRepo.saveAll(appointments);
    }
}
