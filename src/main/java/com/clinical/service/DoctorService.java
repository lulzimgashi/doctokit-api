package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.model.ClinicRole;
import com.clinical.model.CustomUser;
import com.clinical.model.DoctorRole;
import com.clinical.model.User;
import com.clinical.repo.ClinicRoleRepo;
import com.clinical.repo.DoctorRoleRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DoctorService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private DoctorRoleRepo doctorRoleRepo;

    @Autowired
    private ClinicRoleRepo clinicRoleRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional
    public void updateDoctorRoles(Authentication authentication, List<String> clinicRoles, String doctorId) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        String clinicId2 = userRepo.findClinicId(doctorId);

        if (!clinicId.equals(clinicId2)) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_USER_EXCEPTION);
        }


        doctorRoleRepo.dDoctorRoles(doctorId);

        List<DoctorRole> doctorRoles = new ArrayList<>();

        for (String clinicRoleTranslationId : clinicRoles) {
            DoctorRole doctorRole = new DoctorRole();

            doctorRole.setDoctorId(doctorId);
            doctorRole.setRoleTranslationId(clinicRoleTranslationId);

            doctorRoles.add(doctorRole);
        }

        doctorRoleRepo.saveAll(doctorRoles);
    }


    @Transactional(readOnly = true)
    public void getDoctorRoles(List<User> doctors) {

        List<String> doctorsId = new ArrayList<>();


        for (User doctor : doctors) {
            doctorsId.add(doctor.getId());
        }


        Map<String, Object> map = new HashMap<>();
        map.put("doctor_ids", doctorsId);

        namedParameterJdbcTemplate.query(SQL.GET_ALL_DOCTOR_ROLES_FOR_DOCTOR, map, resultSet -> {

            Map<String, List<ClinicRole>> clinicRolesMap = new HashMap<>();

            for (User doctor : doctors) {
                clinicRolesMap.put(doctor.getId(),new ArrayList<>());
            }

            resultSet.beforeFirst();

            while (resultSet.next()) {
                ClinicRole clinicRole = new ClinicRole();

                clinicRole.setId(resultSet.getString("cr.id"));
                clinicRole.setName(resultSet.getString("name"));
                clinicRole.setClinicId(resultSet.getString("clinic_id"));
                clinicRole.setLang(resultSet.getString("lang"));
                clinicRole.setActive(resultSet.getBoolean("active"));
                clinicRole.setDeletedAt(resultSet.getString("deleted_at"));
                clinicRole.setTranslationId(resultSet.getString("translation_id"));

                String doctorId = resultSet.getString("doctor_id");

                if (clinicRolesMap.containsKey(resultSet.getString("doctor_id"))) {
                    clinicRolesMap.get(doctorId).add(clinicRole);
                }
            }

            for (User doctor : doctors) {
                doctor.setClinicRoles(clinicRolesMap.get(doctor.getId()));
            }

        });
    }


    @Transactional(readOnly = true)
    public List<ClinicRole> getDoctorRolesForClinic(Authentication authentication) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();

        return clinicRoleRepo.getAllByClinicId(clinicId);
    }
}
