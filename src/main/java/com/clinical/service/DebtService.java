package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.model.Charge;
import com.clinical.model.CustomUser;
import com.clinical.model.Debt;
import com.clinical.repo.DebtRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 19/04/2018.
 */
@Service
public class DebtService {

    @Autowired
    private DebtRepo debtRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Transactional
    public Debt addDebt(Authentication authentication,Debt debt){
        String id = ((CustomUser)authentication.getPrincipal()).getId();
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();

        debt.setUserId(id);
        debt.setClinicId(clinicId);

        return debtRepo.save(debt);
    }

    @Transactional
    public Debt updateDebt(Authentication authentication, Debt debt) {
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String debtClinicId = debtRepo.findClinicId(debt.getId());
        if(!clinicId.equals(debtClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_DEBT_EXCEPTION);
        }
        String id = ((CustomUser)authentication.getPrincipal()).getId();

        debt.setUserId(id);
        debt.setClinicId(clinicId);

        return debtRepo.save(debt);
    }


    @Transactional
    public void deleteDebt(Authentication authentication, String id) {
        String clinicId= ((CustomUser)authentication.getPrincipal()).getClinicId();
        String debtClinicId=debtRepo.findClinicId(id);

        if(!clinicId.equals(debtClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_DEBT_EXCEPTION);
        }

        String userId= ((CustomUser)authentication.getPrincipal()).getId();

        debtRepo.dDebt(userId,id);

    }


    @Transactional(readOnly = true)
    public List<Debt> getDebtsForDate(Authentication authentication,String type, String date) {
        String clinicId= ((CustomUser)authentication.getPrincipal()).getClinicId();
        Map<String,Object> map = new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("type",type);
        map.put("date",date);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_DEBTS_FOR_CLINIC_SQL,map,resultSet -> {
            List<Debt> debts=new ArrayList<>();

            String last="";
            while(resultSet.next()){
                if(!last.equals(resultSet.getString("d.id"))){
                    Debt debt=new Debt();
                    debt.setId(resultSet.getString("d.id"));
                    debt.setClinicId(clinicId);
                    debt.setAmount(resultSet.getDouble("d.amount"));
                    debt.setDescription(resultSet.getString("d.description"));
                    debt.setName(resultSet.getString("d.name"));
                    debt.setPhone(resultSet.getString("d.phone"));
                    debt.setStartDate(resultSet.getString("d.start_date"));
                    debt.setEndDate(resultSet.getString("d.end_date"));
                    debt.setPatientId(resultSet.getString("d.patient_id"));
                    debt.setChargeList(new ArrayList<>());

                    debts.add(debt);
                }

                if(resultSet.getString("ch.id")!=null) {
                    Charge charge = new Charge();
                    charge.setId(resultSet.getString("ch.id"));
                    charge.setDate(resultSet.getString("ch.date"));
                    charge.setDebtId(resultSet.getString("d.id"));
                    charge.setAmount(resultSet.getDouble("ch.amount"));
                    charge.setDescription(resultSet.getString("ch.description"));
                    charge.setClinicId(clinicId);

                    debts.get(debts.size()-1).getChargeList().add(charge);
                }


                last=resultSet.getString("d.id");
            }


            return debts;
        });
    }


    @Transactional(readOnly = true)
    public List<Debt> debtsForPatient(String patientId,String clinicId){
        Map<String,Object> map=new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("patientId",patientId);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_DEBTS_FOR_PATIENT,map,resultSet -> {
            List<Debt> debts=new ArrayList<>();

            String last="";
            while(resultSet.next()){

                if(!last.equals(resultSet.getString("d.id"))){
                    Debt debt = new Debt();
                    debt.setId(resultSet.getString("d.id"));
                    debt.setAmount(resultSet.getDouble("d.amount"));
                    debt.setDescription(resultSet.getString("d.description"));
                    debt.setEndDate(resultSet.getString("d.end_date"));
                    debt.setStartDate(resultSet.getString("d.start_date"));
                    debt.setUserId(resultSet.getString("d.user_id"));
                    debt.setClinicId(clinicId);
                    debt.setPatientId(patientId);
                    debt.setChargeList(new ArrayList<>());

                    debts.add(debt);
                }

                if(resultSet.getString("ch.id")!=null){
                    Charge charge = new Charge();
                    charge.setId(resultSet.getString("ch.id"));
                    charge.setDebtId(resultSet.getString("d.id"));
                    charge.setDate(resultSet.getString("ch.date"));
                    charge.setAmount(resultSet.getDouble("ch.amount"));
                    charge.setDescription(resultSet.getString("ch.description"));
                    charge.setUserId(resultSet.getString("ch.user_id"));
                    charge.setClinicId(clinicId);

                    debts.get(debts.size()-1).getChargeList().add(charge);
                }


                last=resultSet.getString("d.id");
            }

            return debts;

        });
    }
}
