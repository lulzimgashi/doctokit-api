package com.clinical.service;

import com.clinical.exception.FileNotUploadedException;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Component("uploadService")
public class UploadService {
    private Storage storage;

    @Value("${google.storage.bucket}")
    private String bucket;


    public void init() {
        try {
            storage = StorageOptions.newBuilder()
                    .setProjectId("project")
                    .setCredentials(ServiceAccountCredentials.fromStream(this.getClass().getClassLoader().getResourceAsStream("storageInfo.json")))
                    .build().getService();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getFile(String name) {

        BlobId of = BlobId.of(bucket, name);

        Blob blob = this.storage.get(of);

        URL url = blob.signUrl(2, TimeUnit.MINUTES, Storage.SignUrlOption.httpMethod(HttpMethod.GET));

        return url.toString();
    }

    public String uploadFile(MultipartFile file, String name) throws FileNotUploadedException {


        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileNotUploadedException("File not uploaded");
        }

        BlobInfo builder = BlobInfo.newBuilder(bucket, name).setContentType(file.getContentType()).build();
        Blob blob = storage.create(builder, inputStream);
        URL url = blob.signUrl(4000, TimeUnit.DAYS, Storage.SignUrlOption.httpMethod(HttpMethod.GET));

        return url.toString();
    }

    public void deleteFile(String name){
        BlobId of = BlobId.of(bucket, name);
        storage.delete(of);
    }

}