package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.exception.FileNotUploadedException;
import com.clinical.model.CustomUser;
import com.clinical.model.Document;
import com.clinical.repo.DocumentRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import com.google.common.io.Files;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Service
public class DocumentService {

    @Autowired
    private DocumentRepo documentRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UploadService uploadService;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private DebtService debtService;

    @Transactional
    public Document addDocument(Authentication authentication, String name, MultipartFile file, String label, String parentId, String parentName,String date) throws FileNotUploadedException {
        String originalFilename = file.getOriginalFilename();
        String fileExtension = Files.getFileExtension(originalFilename);
        String filename = name + "." + fileExtension;


        String clinicId =((CustomUser)authentication.getPrincipal()).getClinicId();
        String userId = ((CustomUser)authentication.getPrincipal()).getId();

        Document document=new Document();
        if(parentName.equals("cost")){
            document.setCostId(parentId);
        }else if(parentName.equals("debt")){
            document.setDebtId(parentId);
        }else if(parentName.equals("patient")){
            document.setPatientId(parentId);
        }
        document.setClinicId(clinicId);
        document.setUserId(userId);
        document.setType(fileExtension);
        document.setName(filename);
        document.setLabel(label);
        document.setDate(date);
        String uuid = "33"+ UUID.randomUUID().toString().replace("-","") + "33";
        document.setId(uuid);


        String url = uploadService.uploadFile(file, uuid + "." + fileExtension);

        document.setUrl(url);

        return documentRepo.save(document);
    }


    @Transactional
    public void updateDocument(Authentication authentication,Document document){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String docClinicId= documentRepo.findClinicId(document.getId());

        if(!clinicId.equals(docClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_DOCUMENT_EXCEPTION);
        }

        String userId=((CustomUser)authentication.getPrincipal()).getId();


        documentRepo.updateDocument(document.getName(),document.getLabel(),document.getId(),userId);
    }

    @Transactional
    public void deleteDocument(Authentication authentication,String id){

        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String docClinicId= documentRepo.findClinicId(id);

        if(!clinicId.equals(docClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_DOCUMENT_EXCEPTION);
        }


        Document one = documentRepo.getOne(id);

        uploadService.deleteFile(one.getId()+"."+one.getType());
        documentRepo.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<Document> documentsForPatient(String patientId,String clinicId){
        Map<String,Object> map=new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("patientId",patientId);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_DOCUMENTS_FOR_PATIENT,map, resultSet -> {
            List<Document> documents=new ArrayList<>();

            while (resultSet.next()){
                Document document=new Document();
                document.setId(resultSet.getString("id"));
                document.setLabel(resultSet.getString("label"));
                document.setName(resultSet.getString("name"));
                document.setType(resultSet.getString("type"));
                document.setUrl(resultSet.getString("url"));
                document.setDate(resultSet.getString("date"));
                document.setUserId(resultSet.getString("user_id"));
                document.setClinicId(clinicId);
                document.setPatientId(patientId);


                documents.add(document);
            }

            return documents;
        });
    }
}
