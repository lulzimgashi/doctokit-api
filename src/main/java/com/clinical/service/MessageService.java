package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.model.CustomUser;
import com.clinical.model.Message;
import com.clinical.repo.MessageRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Service
public class MessageService {

    @Autowired
    private MessageRepo messageRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Transactional
    public Message addMessage(Authentication authentication,Message message){
        String id =((CustomUser)authentication.getPrincipal()).getId();
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();

        message.setUserId(id);
        message.setClinicId(clinicId);

        return messageRepo.save(message);
    }


    @Transactional
    public void deleteMessage(Authentication authentication,String id){
        String clinicId =((CustomUser)authentication.getPrincipal()).getClinicId();
        String messageClinicId=messageRepo.findClinicId(id);

        if(!clinicId.equals(messageClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_MESSAGE_EXCEPTION);
        }

        String userId =((CustomUser)authentication.getPrincipal()).getId();

        messageRepo.dMEssage(userId,id);
    }


    @Transactional(readOnly = true)
    public List<Message> messagesForPatient(String patientId,String clinicId){
        Map<String,Object> map=new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("patientId",patientId);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_MESSAGES_FOR_PATIENT,map,resultSet -> {
           List<Message> messages=new ArrayList<>();

           while(resultSet.next()){
               Message message=new Message();
               message.setId(resultSet.getString("id"));
               message.setDate(resultSet.getString("date"));
               message.setPhone(resultSet.getLong("phone"));
               message.setText(resultSet.getString("text"));
               message.setUserId(resultSet.getString("user_id"));
               message.setClinicId(clinicId);
               message.setPatientId(patientId);

               messages.add(message);
           }

           return messages;
        });
    }
}
