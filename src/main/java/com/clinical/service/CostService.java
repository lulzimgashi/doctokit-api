package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.model.Cost;
import com.clinical.model.CustomUser;
import com.clinical.repo.CostRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Service
public class CostService {

    @Autowired
    private CostRepo costRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Transactional
    public Cost addCost(Authentication authentication,Cost cost){
        String clinicId =((CustomUser)authentication.getPrincipal()).getClinicId();
        String id = ((CustomUser)authentication.getPrincipal()).getId();

        cost.setClinicId(clinicId);
        cost.setUserId(id);


        return costRepo.save(cost);
    }


    @Transactional
    public Cost updateCost(Authentication authentication,Cost cost){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String costClinicId= costRepo.findClinicId(cost.getId());

        if(!clinicId.equals(costClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_COST_EXCEPTION);
        }
        String id = ((CustomUser)authentication.getPrincipal()).getId();


        cost.setUserId(id);
        cost.setClinicId(clinicId);
        return costRepo.save(cost);
    }


    @Transactional
    public void deleteCost(Authentication authentication,String id){
        String clinicId =((CustomUser)authentication.getPrincipal()).getClinicId();
        String costClinicId= costRepo.findClinicId(id);

        if(!clinicId.equals(costClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_COST_EXCEPTION);
        }

        String userId = ((CustomUser)authentication.getPrincipal()).getId();

        costRepo.dCost(userId,id);
    }


    @Transactional(readOnly = true)
    public List<Cost> getAllCostsForDate(Authentication authentication,String date){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        Map<String,Object> map = new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("date",date);


        return namedParameterJdbcTemplate.query(SQL.GET_ALL_COSTS_FOR_CLINIC_SQL,map, resultSet -> {
            List<Cost> costs=new ArrayList<>();


            while(resultSet.next()){
                Cost cost=new Cost();
                cost.setId(resultSet.getString("id"));
                cost.setAmount(resultSet.getDouble("amount"));
                cost.setDate(resultSet.getString("date"));
                cost.setDescription(resultSet.getString("description"));
                cost.setName(resultSet.getString("name"));
                cost.setPhone(resultSet.getString("phone"));
                cost.setUserId(resultSet.getString("user_id"));
                cost.setClinicId(clinicId);

                costs.add(cost);
            }

            return costs;
        });

    }
}
