package com.clinical.service;

import com.clinical.model.CustomUser;
import com.clinical.model.Service;
import com.clinical.repo.ServiceRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;

import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lulzimgashi on 22/04/2018.
 */
@org.springframework.stereotype.Service
public class ServiceService {

    @Autowired
    private ServiceRepo serviceRepo;


    @Transactional
    public Service addService(Authentication authentication, Service service) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();

        service.setClinicId(clinicId);

        return serviceRepo.save(service);
    }

    @Transactional
    public void updateService(Authentication authentication, Service service) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        String sClinicId = serviceRepo.findClinicId(service.getId());

        if (!clinicId.equals(sClinicId)) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_SERVICE_EXCEPTION);
        }

        serviceRepo.update(service.getActive(), service.getName(), service.getId());
    }

    @Transactional
    public void deleteService(Authentication authentication, String id) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        String sClinicId = serviceRepo.findClinicId(id);

        if (!clinicId.equals(sClinicId)) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_SERVICE_EXCEPTION);
        }

        serviceRepo.dService(id);
    }


    public List<Service> getServicesForClinic(String id) {
        List list = serviceRepo.findAllByClinicIdAndDeletedAtIsNull(id);
        if (list != null) {
            return list;
        } else {
            return new ArrayList<>();
        }
    }
}
