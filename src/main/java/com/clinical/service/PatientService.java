package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.exception.PatientExistException;
import com.clinical.model.Appointment;
import com.clinical.model.CustomUser;
import com.clinical.model.Patient;
import com.clinical.repo.PatientRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Service
public class PatientService {

    @Autowired
    private PatientRepo patientRepo;


    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    private NoteService noteService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private DebtService debtService;

    @Autowired
    private AnamnesisService anamnesisService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private AppointmentService appointmentService;



    @Transactional
    public Patient addPatient(Authentication authentication, Patient patient) throws PatientExistException {
        String maybeDisabledId = checkIfPatientExist(patient);

        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();

        if (maybeDisabledId != null) {
            patient.setId(maybeDisabledId);
        }
        patient.setClinicId(clinicId);
        patient.setEnabled(true);

        Patient save = patientRepo.save(patient);

        if(patient.getAppointments() != null && patient.getAppointments().size() > 0){

            for (Appointment appointment : patient.getAppointments()) {
                appointment.setClinicId(clinicId);
                appointment.setUserId(((CustomUser)authentication.getPrincipal()).getId());
                appointment.setPatientId(save.getId());
            }

            save.setAppointments(appointmentService.addAppointmentFromPatient(patient.getAppointments()));
        }


        return save;
    }

    @Transactional
    public Patient updatePatient(Authentication authentication, Patient patient) throws PatientExistException {
        String maybeDisabled = checkIfPatientExist(patient);

        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        if (!clinicId.equals(patient.getClinicId())) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_PATIENT_EXCEPTION);
        }

        if (maybeDisabled != null) {
            patient.setId(maybeDisabled);
        }
        patient.setEnabled(true);
        patient.setClinicId(clinicId);
        return patientRepo.save(patient);
    }

    @Transactional
    public void deletePatient(Authentication authentication, String patientId) {
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        if (!clinicId.equals(patientRepo.findClinicId(patientId))) {
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_PATIENT_EXCEPTION);
        }

        patientRepo.dPatient(patientId);
    }

    @Transactional(readOnly = true)
    public List<Patient> searchPatientFast(Authentication authentication, Integer limit, String value) {
        return patientRepo.searchPatientsFast(value, limit, ((CustomUser)authentication.getPrincipal()).getClinicId());
    }

    @Transactional(readOnly = true)
    public List<Patient> searchAllPatients(Authentication authentication) {
        return patientRepo.getAllPatients(((CustomUser)authentication.getPrincipal()).getClinicId());
    }


    @Transactional(readOnly = true)
    public Patient getPatient(Authentication authentication,String id){
        String clinicId=((CustomUser)authentication.getPrincipal()).getClinicId();
        Map<String,Object> map=new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("id",id);



        Patient patient = namedParameterJdbcTemplate.query(SQL.GET_PATIENT_WITH_ID,map, resultSet -> {
            Patient obj=new Patient();

            boolean checked=false;
            while (resultSet.next()){
                if(!checked){
                    obj.setId(resultSet.getString("p.id"));
                    obj.setAddress(resultSet.getString("p.address"));
                    obj.setBirthDate(resultSet.getString("p.birth_date"));
                    obj.setFirstName(resultSet.getString("p.first_name"));
                    obj.setLastName(resultSet.getString("p.last_name"));
                    obj.setPhone(resultSet.getLong("p.phone"));
                    obj.setGender(resultSet.getString("p.gender"));
                    obj.setEmail(resultSet.getString("p.email"));
                    obj.setEnabled(resultSet.getBoolean("p.enabled"));

                    obj.setAppointments(new ArrayList<>());
                }

                if(resultSet.getString("a.id") !=null){
                    Appointment appointment=new Appointment();
                    appointment.setId(resultSet.getString("a.id"));
                    appointment.setCreatedAt(resultSet.getString("a.created_at"));
                    appointment.setDate(resultSet.getString("a.date"));
                    appointment.setStart(resultSet.getString("a.start"));
                    appointment.setEnd(resultSet.getString("a.end"));
                    appointment.setDescription(resultSet.getString("a.description"));
                    appointment.setDoctorId(resultSet.getString("a.doctor_id"));
                    appointment.setPatientId(resultSet.getString("p.id"));
                    appointment.setClinicId(clinicId);
                    appointment.setUserId(resultSet.getString("a.user_id"));

                    obj.getAppointments().add(appointment);
                }

                checked=true;
            }

            return obj;
        });


        patient.setNotes(noteService.notesForPatient(id,clinicId));
        patient.setDocuments(documentService.documentsForPatient(id,clinicId));
        patient.setDebts(debtService.debtsForPatient(id,clinicId));
        patient.setAnamneses(anamnesisService.anamnesesForPatient(id,clinicId));
        patient.setMessages(messageService.messagesForPatient(id,clinicId));

        patient.setClinicId(clinicId);
        return patient;
    }


    private String checkIfPatientExist(Patient patient) throws PatientExistException {
        if (patient.getId() == null) {
            patient.setId("0");
        }
        List<Patient> founds = patientRepo.findIfPatientExist(patient.getPhone(), patient.getEmail(), patient.getId());

        for (Patient found : founds) {
            if (found.getPhone().equals(patient.getPhone()) && found.getEmail().equals(patient.getEmail()) && !found.getEnabled()) {
                return found.getId();
            } else if (found.getPhone().equals(patient.getPhone())) {
                throw new PatientExistException(String.format(ConstValues.PATIENT_WITH_SAME_FIELD, "phone", found.getPhone().toString(), "phone"));
            } else if (patient.getEmail().length()>0 && found.getEmail().equals(patient.getEmail())) {
                throw new PatientExistException(String.format(ConstValues.PATIENT_WITH_SAME_FIELD, "email", found.getEmail(), "email"));
            }
        }
        return null;
    }

}
