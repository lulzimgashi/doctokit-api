package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.exception.BadAuthorityException;
import com.clinical.exception.UserExistException;
import com.clinical.model.*;
import com.clinical.repo.ClinicRepo;
import com.clinical.repo.GroupMemberRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import com.clinical.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by lulzimgashi on 14/04/2018.
 */
@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private GroupMemberRepo groupMemberRepo;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private EmailService emailService;

    @Autowired
    private ClinicService clinicService;


    @Transactional(readOnly = true)
    public List<User> getAllUsers(Authentication authentication) {
        String clinicId =((CustomUser)authentication.getPrincipal()).getClinicId();

        return jdbcTemplate.query(SQL.GET_ALL_USERS_FOR_CLINIC, new Object[]{clinicId}, resultSet -> {

            List<User> users = new ArrayList<>();

            String last = "";
            while (resultSet.next()) {
                if (!last.equals(resultSet.getString("id"))) {
                    User user = new User();
                    user.setId(resultSet.getString("id"));
                    user.setFirstName(resultSet.getString("first_name"));
                    user.setLastName(resultSet.getString("last_name"));
                    user.setEmail(resultSet.getString("email"));
                    user.setPhone(resultSet.getLong("phone"));
                    user.setEnabled(resultSet.getBoolean("enabled"));
                    user.setClinicId(clinicId);
                    user.setPassword("PROTECTED");
                    user.setAuthorities(new ArrayList<>());
                    user.setDoctor(resultSet.getBoolean("doctor"));

                    users.add(user);
                }

                users.get(users.size() - 1).getAuthorities().add(resultSet.getString("authority"));
                last = resultSet.getString("id");
            }

            return users;
        });

    }


    @Transactional
    public User addUser(Authentication authentication, User user) throws UserExistException, BadAuthorityException {
        boolean isSuperAdmin = UserUtils.isSuperAdmin(authentication);

        if ((user.getAuthorities().contains("SUPER_ADMIN") || user.getAuthorities().contains("ADMIN")) && !isSuperAdmin) {
            throw new AccessDeniedException(String.format(ConstValues.CANNOT_ADD_WITH_ROLE, "Admin"));
        }
        checkUserIfExist(user);
        UserUtils.checkAuthorities(user.getAuthorities());
        List<GroupMember> groupMembers = UserUtils.modifyAuthorities(user.getAuthorities(), user.getEmail());
        groupMemberRepo.saveAll(groupMembers);

        if (!isSuperAdmin) {
            user.setClinicId(((CustomUser)authentication.getPrincipal()).getClinicId());
        }
        user.setUsername(user.getEmail());
        user.setEnabled(true);
        user.setDoctor(false);
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userRepo.save(user);
    }


    @Transactional
    public User updateUser(Authentication authentication, User user) throws UserExistException, BadAuthorityException {
        boolean isSuperAdmin = UserUtils.isSuperAdmin(authentication);

        String clinicOfAuthentication = ((CustomUser)authentication.getPrincipal()).getClinicId();

        if (!isSuperAdmin && !clinicOfAuthentication.equals(user.getClinicId())) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_USER_EXCEPTION);
        }

        if (user.getAuthorities().contains("SUPER_ADMIN") && !isSuperAdmin) {
            throw new AccessDeniedException(String.format(ConstValues.CANNOT_ADD_WITH_ROLE, "Super Admin"));
        }

        checkUserIfExist(user);
        UserUtils.checkAuthorities(user.getAuthorities());

        List<GroupMember> groupMembers = UserUtils.modifyAuthorities(user.getAuthorities(), user.getEmail());
        groupMemberRepo.deleteForUser(userRepo.findUsername(user.getId()));
        groupMemberRepo.saveAll(groupMembers);


        if (!isSuperAdmin) {
            user.setClinicId(clinicOfAuthentication);
        }

        user.setUsername(user.getEmail());
        user.setPassword(userRepo.findPassword(user.getId()));
        user.setEnabled(true);
        return userRepo.save(user);
    }

    @Transactional
    public void deleteUser(Authentication authentication, String id) {
        String clinicId= ((CustomUser)authentication.getPrincipal()).getClinicId();
        if (!clinicId.equals(userRepo.findClinicId(id))) {
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_USER_EXCEPTION);
        }
        userRepo.dUser(id);
    }

    @Transactional
    public void changePassword(Authentication authentication, PasswordPayload passwordPayload) {
        String clinicId= ((CustomUser)authentication.getPrincipal()).getClinicId();
        if (!clinicId.equals(userRepo.findClinicId(passwordPayload.getUserId()))) {
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_USER_EXCEPTION);
        }

        userRepo.changePassword(new BCryptPasswordEncoder().encode(passwordPayload.getPassword()), passwordPayload.getUserId());
    }

    @Transactional
    public void resetPassword(PasswordPayload passwordPayload) {
        User user = userRepo.findByUsername(passwordPayload.getUserId());
        if (user != null) {
            Random random = new Random();
            String generatedPassword = String.format("%04d", random.nextInt(10000));
            userRepo.changePassword(new BCryptPasswordEncoder().encode(generatedPassword), user.getId());

            Mail mail = new Mail();
            mail.setSubject("Reset Password");
            mail.setContent(String.format("Your new password is : %s", generatedPassword));
            mail.setTo(user.getEmail());
            mail.setFrom("gshlulzim@gmail.com");

            emailService.sendSimpleMessage(mail);
        }
    }

    @Transactional
    public void updateDoctor(Authentication authentication,String id,Boolean value){
        String clinicId= ((CustomUser)authentication.getPrincipal()).getClinicId();
        String clinicId2 = userRepo.findClinicId(id);


        if(!clinicId.equals(clinicId2)){
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_USER_EXCEPTION);
        }

        userRepo.updateDoctor(id,value);
    }

    @Transactional(readOnly = true)
    public User getUser(Authentication authentication) {
        User user = userRepo.findByUsername(authentication.getName());
        user.setPassword("PROTECTED");
        user.setAuthorities(new ArrayList<>());
        if(user.getClinicId()!=null){
           Clinic clinic = clinicService.getClinicById(user.getClinicId());
           user.setClinic(clinic);

           user.setAuthorities(getAuthorities(user.getEmail()));
        }
        return user;
    }


    private void checkUserIfExist(User user) throws UserExistException {
        if (user.getId() == null) {
            user.setId("0");
        }
        List<User> foundedUsers = userRepo.findIfUserExist(user.getEmail(), user.getPhone(), user.getId());
        for (User foundedUser : foundedUsers) {
            if (foundedUser.getEmail().equals(user.getEmail())) {
                throw new UserExistException(String.format(ConstValues.USER_WITH_SAME_FIELD, "email", user.getEmail(), "email"));
            } else if (foundedUser.getPhone().equals(user.getPhone())) {
                throw new UserExistException(String.format(ConstValues.USER_WITH_SAME_FIELD, "phone number", user.getPhone().toString(), "phone number"));
            }
        }
    }

    private List<String> getAuthorities(String username){
        return jdbcTemplate.query(SQL.GET_AUTHORITIES_FOR_USER,new Object[]{username},resultSet -> {
            List<String> authorities=new ArrayList<>();

            while (resultSet.next()){
                authorities.add(resultSet.getString("authority"));
            }

            return authorities;
        });
    }

}
