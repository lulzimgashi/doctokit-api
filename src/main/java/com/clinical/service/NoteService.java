package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.model.CustomUser;
import com.clinical.model.Note;
import com.clinical.repo.NoteRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lulzimgashi on 18/04/2018.
 */
@Service
public class NoteService {

    @Autowired
    private NoteRepo noteRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Transactional
    public Note addNote(Authentication authentication, Note note) {
        String clinicId =  ((CustomUser)authentication.getPrincipal()).getClinicId();
        String id =  ((CustomUser)authentication.getPrincipal()).getId();

        note.setClinicId(clinicId);
        note.setUserId(id);

        return noteRepo.save(note);
    }


    @Transactional
    public Note updateNote(Authentication authentication,Note note){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        if(!clinicId.equals(note.getClinicId())){
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_NOTE_EXCEPTION);
        }

        String id = ((CustomUser)authentication.getPrincipal()).getId();
        note.setUserId(id);
        note.setClinicId(clinicId);

        return noteRepo.save(note);
    }


    @Transactional
    public void deleteNote(Authentication authentication,String id){
        String clinicId =  ((CustomUser)authentication.getPrincipal()).getClinicId();
        String noteClinicId = noteRepo.findClinicId(id);
        if(!clinicId.equals(noteClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_NOTE_EXCEPTION);
        }

        String userId= ((CustomUser)authentication.getPrincipal()).getId();

        noteRepo.dNote(userId,id);
    }


    @Transactional(readOnly = true)
    public List<Note> notesForPatient(String patientId,String clinicId){
        Map<String,Object> map=new HashMap<>();
        map.put("clinicId",clinicId);
        map.put("patientId",patientId);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_NOTES_FOR_PATIENT,map,resultSet -> {
            List<Note> notes=new ArrayList<>();

            while(resultSet.next()){
                Note note = new Note();
                note.setId(resultSet.getString("id"));
                note.setDate(resultSet.getString("date"));
                note.setText(resultSet.getString("text"));
                note.setClinicId(clinicId);
                note.setPatientId(patientId);
                note.setUserId(resultSet.getString("user_id"));
                note.setServiceId(resultSet.getString("service_id"));

                notes.add(note);
            }

            return notes;
        });

    }

}
