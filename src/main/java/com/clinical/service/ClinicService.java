package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.exception.FileNotUploadedException;
import com.clinical.model.Clinic;
import com.clinical.model.CustomUser;
import com.clinical.model.Day;
import com.clinical.repo.ClinicRepo;
import com.clinical.repo.DayRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lulzimgashi on 15/04/2018.
 */
@Service
public class ClinicService {

    @Autowired
    private ClinicRepo clinicRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private DayRepo dayRepo;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UploadService uploadService;

    @Transactional
    public Clinic addClinic(Clinic clinic) {
        Clinic saved = clinicRepo.save(clinic);

        for (Day day : clinic.getDays()) {
            day.setClinicId(saved.getId());
        }

        dayRepo.saveAll(clinic.getDays());

        clinic.setId(saved.getId());
        return clinic;
    }

    @Transactional
    public Clinic updateClinic(Authentication authentication, Clinic clinic) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();

        if (!clinicId.equals(clinic.getId())) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_CLINIC_EXCEPTION);
        }

        return clinicRepo.save(clinic);
    }

    @Transactional(readOnly = true)
    public Clinic getClinic(Authentication authentication) {
        String clinicId = "";
        if (authentication != null) {
            clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        }

        Clinic clinic = clinicRepo.findById(clinicId).get();
        clinic.setDays(dayRepo.findAllByClinicId(clinicId));

        return clinic;
    }

    @Transactional
    public void deleteClinic(String clinicId) {
        clinicRepo.dClinic(clinicId);
    }


    @Transactional
    public void updateClinicDays(Authentication authentication, Day day) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();

        dayRepo.save(day);
    }

    @Transactional(readOnly = true)
    public Clinic getClinicById(String clinicId) {

        Clinic dbClinic = jdbcTemplate.query(SQL.GET_CLINIC_BY_ID, new Object[]{clinicId}, resultSet -> {
            Clinic clinic = new Clinic();


            boolean checked = false;
            while (resultSet.next()) {
                if (!checked) {
                    clinic.setId(resultSet.getString("c.id"));
                    clinic.setAddress(resultSet.getString("c.address"));
                    clinic.setLocation(resultSet.getString("c.location"));
                    clinic.setCoverImage(resultSet.getString("c.cover_image"));
                    clinic.setDescription(resultSet.getString("c.description"));
                    clinic.setEmail(resultSet.getString("c.email"));
                    clinic.setEnabled(resultSet.getBoolean("c.enabled"));
                    clinic.setLatitude(resultSet.getDouble("c.latitude"));
                    clinic.setLongitude(resultSet.getDouble("c.longitude"));
                    clinic.setName(resultSet.getString("c.name"));
                    clinic.setPhone(resultSet.getLong("c.phone"));
                    clinic.setProfileImage(resultSet.getString("c.profile_image"));
                    clinic.setSlotTime(resultSet.getInt("c.slot_time"));
                    clinic.setSms(resultSet.getBoolean("c.sms"));
                    clinic.setWebsite(resultSet.getString("c.website"));
                    clinic.setLanguage(resultSet.getString("c.language"));
                    clinic.setType(resultSet.getString("c.type"));

                    clinic.setDays(new ArrayList<>());
                }

                if (resultSet.getString("d.id") != null) {
                    Day day = new Day();
                    day.setId(resultSet.getString("d.id"));
                    day.setClinicId(clinicId);
                    day.setDay(resultSet.getInt("d.day"));
                    day.setEnd(resultSet.getString("d.end"));
                    day.setStart(resultSet.getString("d.start"));
                    day.setOpen(resultSet.getBoolean("d.open"));

                    clinic.getDays().add(day);
                }


                checked = true;
            }

            return clinic;
        });

        dbClinic.setServices(serviceService.getServicesForClinic(dbClinic.getId()));

        return dbClinic;
    }

    @Transactional
    public String addImage(Authentication authentication, String type, MultipartFile file) throws FileNotUploadedException {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();

        String name = clinicId + type + ".png";

        String url = uploadService.uploadFile(file, name);

        String column = type.equals("cover") ? "cover_image" : "profile_image";

        clinicRepo.updateImg(url, column, clinicId);

        return url;
    }
}
