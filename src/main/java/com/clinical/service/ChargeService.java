package com.clinical.service;

import com.clinical.model.Charge;
import com.clinical.model.CustomUser;
import com.clinical.repo.ChargeRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by lulzimgashi on 19/04/2018.
 */
@Service
public class ChargeService {

    @Autowired
    private ChargeRepo chargeRepo;

    @Autowired
    private UserRepo userRepo;



    @Transactional
    public Charge addCharge(Authentication authentication,Charge charge){
        String clinicId =((CustomUser)authentication.getPrincipal()).getClinicId();
        String id = ((CustomUser)authentication.getPrincipal()).getId();

        charge.setClinicId(clinicId);
        charge.setUserId(id);

        return chargeRepo.save(charge);
    }


    @Transactional
    public Charge updateCharge(Authentication authentication,Charge charge){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String chargeClinicId = chargeRepo.findClinicId(charge.getId());

        if(!clinicId.equals(chargeClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_CHARGE_EXCEPTION);
        }

        String userId =((CustomUser)authentication.getPrincipal()).getId();

        charge.setUserId(userId);
        charge.setClinicId(clinicId);

        return chargeRepo.save(charge);
    }


    @Transactional
    public void deleteCharge(Authentication authentication,String id){
        String clinicId = ((CustomUser)authentication.getPrincipal()).getClinicId();
        String chargeClinicId = chargeRepo.findClinicId(id);

        if(!clinicId.equals(chargeClinicId)){
            throw new AccessDeniedException(ConstValues.CANNOT_DELETE_CHARGE_EXCEPTION);
        }
        String userId =((CustomUser)authentication.getPrincipal()).getId();

        chargeRepo.dCharge(userId,id);
    }

}
