package com.clinical.service;

import com.clinical.db.SQL;
import com.clinical.model.Anamnesis;
import com.clinical.model.CustomUser;
import com.clinical.repo.AnamnesisRepo;
import com.clinical.repo.UserRepo;
import com.clinical.utils.ConstValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by lulzimgashi on 21/04/2018.
 */
@Service
public class AnamnesisService {

    @Autowired
    private AnamnesisRepo anamnesisRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Transactional
    public Anamnesis addAnamnesis(Authentication authentication, Anamnesis anamnesis) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        String userId = ((CustomUser) authentication.getPrincipal()).getId();

        anamnesis.setClinicId(clinicId);
        anamnesis.setUserId(userId);

        return anamnesisRepo.save(anamnesis);
    }

    @Transactional
    public Anamnesis updateAnamnesis(Authentication authentication, Anamnesis anamnesis) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        String anaClinicId = anamnesisRepo.findClinicId(anamnesis.getId());

        if (!clinicId.equals(anaClinicId)) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_PATIENT_EXCEPTION);
        }

        String userId = ((CustomUser) authentication.getPrincipal()).getId();

        anamnesis.setClinicId(clinicId);
        anamnesis.setUserId(userId);

        return anamnesisRepo.save(anamnesis);
    }

    @Transactional
    public void deleteAnamnesis(Authentication authentication, String id) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();
        String anaClinicId = anamnesisRepo.findClinicId(id);

        if (!clinicId.equals(anaClinicId)) {
            throw new AccessDeniedException(ConstValues.CANNOT_EDIT_PATIENT_EXCEPTION);
        }

        String userId = ((CustomUser) authentication.getPrincipal()).getId();

        anamnesisRepo.dAnamnesis(userId, id);
    }


    @Transactional(readOnly = true)
    public List<Anamnesis> anamnesesForPatient(String patientId, String clinicId) {
        Map<String, Object> map = new HashMap<>();
        map.put("clinicId", clinicId);
        map.put("patientId", patientId);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_ANAMNESES_FOR_PATIENT, map, resultSet -> {
            List<Anamnesis> anamneses = new ArrayList<>();

            while (resultSet.next()) {
                Anamnesis anamnesis = new Anamnesis();
                anamnesis.setId(resultSet.getString("id"));
                anamnesis.setDate(resultSet.getString("date"));
                anamnesis.setName(resultSet.getString("date"));
                anamnesis.setUserId(resultSet.getString("user_id"));
                anamnesis.setClinicId(clinicId);
                anamnesis.setPatientId(patientId);

                anamneses.add(anamnesis);
            }

            return anamneses;
        });
    }

    @Transactional(readOnly = true)
    public List<String> getAnamneses(Authentication authentication) {
        String clinicId = ((CustomUser) authentication.getPrincipal()).getClinicId();

        Map<String, Object> map = new HashMap<>();
        map.put("clinicId", clinicId);

        return namedParameterJdbcTemplate.query(SQL.GET_ALL_ANAMNESES_FOR_CLINIC, map, resultSet -> {
            List<String> anamneses = new ArrayList<>();

            while(resultSet.next()){
                anamneses.add(resultSet.getString("name"));
            }

            return anamneses;
        });
    }
}
