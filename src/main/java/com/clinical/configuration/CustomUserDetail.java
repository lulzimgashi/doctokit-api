package com.clinical.configuration;

import com.clinical.model.CustomUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lulzimgashi on 22/04/2018.
 */
public class CustomUserDetail implements UserDetailsService {

    private static final String load_by_username_query = "SELECT ga.authority, u.id, u.clinic_id, u.username, u.password FROM group_members gm INNER JOIN group_authorities ga ON ga.group_id = gm.group_id INNER JOIN users u ON u.username = gm.username WHERE gm.username = ? aND u.enabled=1;";


    private JdbcTemplate jdbcTemplate=new JdbcTemplate();

    public CustomUserDetail(DataSource dataSource) {
        jdbcTemplate.setDataSource(dataSource);
    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {


        CustomUser loggedUser = jdbcTemplate.query(load_by_username_query, new Object[]{s}, resultSet -> {
            String username = null;
            String password = null;
            String clinicId = null;
            String id = null;

            List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

            boolean checked = false;
            while (resultSet.next()) {
                if (!checked) {
                    username = resultSet.getString("username");
                    password = resultSet.getString("password");
                    clinicId = resultSet.getString("clinic_id");
                    id = resultSet.getString("u.id");
                }

                if (resultSet.getString("authority") != null) {
                    grantedAuthorities.add(new SimpleGrantedAuthority(resultSet.getString("authority")));
                }

                checked = true;
            }

            CustomUser user = new CustomUser(username, password, grantedAuthorities);
            user.setId(id);
            user.setClinicId(clinicId);

            return user;
        });


        if(loggedUser.getId()==null){
            throw new UsernameNotFoundException("This user doesn't exist");
        }


        return loggedUser;
    }
}
